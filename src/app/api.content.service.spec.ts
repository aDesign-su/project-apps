/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ApicontentService } from './api.content.service';

describe('Service: Apicontent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApicontentService]
    });
  });

  it('should ...', inject([ApicontentService], (service: ApicontentService) => {
    expect(service).toBeTruthy();
  }));
});
