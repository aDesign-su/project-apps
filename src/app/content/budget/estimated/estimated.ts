export interface Estimated {
    id: number,
    id_assoi: string,
    id_SAP: string,
    id_srd: string,
    title: string,
    version: string,
    comments: string,
    macro: number,
    cost: number,
    currency: number,
    position: number
}
export interface workPackage {
    id: number,
    title: string,
    type: number,
    contractor: string,
    cost_total: number,
    cost_pp: number,
    mh: number,
    comments: string,
    project: number,
    region: number,
    estimated_budget: null
}
export interface analog { 
    id: number,
    title: string,
    constr_smr: number,
    date_start: string,
    date_end: string,
    eng_pir: number,
    tech_description: string,
    cost: number,
    type: number,
    region: number,
    estimated_budget: null
}
export interface equipment {
    id: number,
    title: string,
    type: string,
    tech_description: string,
    cost: number,
    description: string,
    project: number,
    manufacturer: number,
    estimated_budget: number
 }