export interface analogBudgets {
    id: number
    title: string,
    constr_smr: number,
    date_start: string,
    date_end: string,
    eng_pir: number,
    tech_description: string,
    cost: number,
    type: string,
    region:string,
    description:string,
    project:string,
    manufacturer:string,
}
export interface WorkPackages {
    id: number
    title: string,
    type: string,
    contractor: string,
    cost_total: number,
    cost_pp: number,
    mh: number,
    comments: string,
    project: number,
    region: number
}
export interface Equipments {
    id: number
    title: string,
    type: string,
    tech_description: string,
    cost: number,
    description: string,
    project: number,
    manufacturer: number,
    estimated_budget: number
}