import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { Observable, map, distinctUntilChanged, switchMap, of  } from 'rxjs';
import { AuthService } from './../../auth.service';

@Component({
  selector: 'app-restore',
  templateUrl: './restore.component.html',
  styleUrls: ['./restore.component.css']
})
export class RestoreComponent implements OnInit {

  @ViewChild('stepper') stepper!: MatStepper;

  resetPasswordForm: FormGroup;
  secondFormGroup: FormGroup;

  isLinear = true;
  emailNotFoundError = false;
  messageError = '';

  constructor(public AuthService: AuthService,private formBuilder: FormBuilder) {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email], this.emailExistsValidator()],
      emailNotFoundError: [false]
    });
    this.secondFormGroup = this.formBuilder.group({
      token: ['', [Validators.required]],
      password: ['', Validators.required,this.characterLengthValidator(8, 20)],
      confirmed_password: ['', Validators.required,this.characterLengthValidator(8, 20)],
    });

  }
  // Шаг первый
  level1(){
    if (this.resetPasswordForm.invalid) {
      return;
    }

  const email = this.resetPasswordForm.get('email')?.value;
  this.AuthService.resetPassword(email).subscribe(
    () => {
      if (!this.emailNotFoundError) {
        this.stepper.selectedIndex = 1; // Перейти на следующий шаг
      }
      // Успешно отправлен запрос на восстановление пароля
      // Выполните здесь дополнительные действия, например, отображение сообщения пользователю
    },
    (error) => {
      if (error.status === 500) {
        this.emailNotFoundError = true;
      }
      this.stepper.selectedIndex = 0; // Останется на первом шаге в случае, если запрос не ушёл
    })
  }

  // Шаг второй
  level2(){
    if (this.secondFormGroup.invalid) {
      // Проверка на валидность полей формы
      return;
    }
  const token = this.secondFormGroup.get('token')?.value;
  const password = this.secondFormGroup.get('password')?.value;
  const confirmed_password = this.secondFormGroup.get('confirmed_password')?.value;

  if (password !== confirmed_password) {
    // Обработка ошибки, если пароли не совпадают
    return;
  }
  this.AuthService.restorePassword(token,password,confirmed_password).subscribe(
    (response) => {
      console.log(response)
      // Успешно отправлен запрос на восстановление пароля
      // Выполните здесь дополнительные действия, например, отображение сообщения пользователю
    },
    (error) => {
      if (error.status === 500) {
        this.emailNotFoundError = true;
      }
    })
  }

  // Проверка на длинну символов
  characterLengthValidator(minLength: number, maxLength: number): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      const value = control.value;
      const length = value ? value.length : 0;
  
      if (length >= minLength && length <= maxLength) {
        return of(null); // Возвращаем Observable с null, если длина символов находится в заданных пределах
      } else {
        return of({ invalidLength: true }); // Возвращаем Observable с объектом ошибки, если длина символов не соответствует требованиям
      }
    };
  }
  // Валидатор для проверки email на совпадение с БД
  emailExistsValidator(): AsyncValidatorFn {
    let isRequestPending = false; // Переменная для отслеживания состояния запроса
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      if (!isRequestPending) {
        isRequestPending = true; // Установить флаг, чтобы предотвратить повторный запрос
        return this.AuthService.resetPassword(control.value).pipe(
          map((exists: boolean) => {
            isRequestPending = false; // Сбросить флаг после завершения запроса
            return exists ? null : { emailNotExists: true };
          })
        );
      } else {
        return of(null); // Возвращаем Observable с null, чтобы не выполнять запрос повторно
      }
    };
  }
  
  restore() {
    console.log('access')
  }

  ngOnInit() {
  }

}
