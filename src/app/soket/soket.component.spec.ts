/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SoketComponent } from './soket.component';

describe('SoketComponent', () => {
  let component: SoketComponent;
  let fixture: ComponentFixture<SoketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
