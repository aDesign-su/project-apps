import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'src/app/theme.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { ApicontentService } from 'src/app/api.content.service';
import {
  DirectionFilter,
  ObjectFilter,
  ProjectFilter,
  WorkFilter,
  SystemFilter,
  EstimateFilter,
  Wbs,
  WbsTable,
  ElementFilter,
  WbsElement, PackageElements, DeleteEntity, CreateElement
} from './wbs';
import {AuthenticationService} from '../../../authentication/login/login/authentication.service'
import { FormBuilder, FormGroup } from '@angular/forms';
// import jmespath from 'jmespath';
import { MatTableDataSource } from '@angular/material/table';
// import { elementNames } from 'dom-serializer/lib/foreignNames'
// import convert from 'lodash/fp/convert'

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
  id: number;
  type_node: string;
  start_date: string;
  end_date: string;
  percent_date: string;
  code_wbs: string;
}

@Component({
  selector: 'app-wbs',
  templateUrl: './wbs.component.html',
  styleUrls: ['./wbs.component.css'],

})

export class WbsComponent implements OnInit {
  constructor(public theme: ThemeService, private wbsService: ApicontentService, private formBuilder: FormBuilder, auth: AuthenticationService) {
    this.ProjectFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: 0,
      user_created: 1,
      package: 0,
    });
    this.DirectionFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: '',
      user_created: 1,
      package: 0,
    });
    this.ObjectFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: '',
      user_created: 1,
      package: 0,
    });
    this.SystemFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: '',
      user_created: 1,
      package: 0,
    });
    this.WorkFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: '',
      user_created: 1,
      package: 0,
    });
    this.EstimateFilters = this.formBuilder.group({
      date_created: '',
      name: '',
      delete_flg: false,
      сode_wbs: 0,
      node_parent: '',
      user_created: 1,
      package: 0,
    });

    this.treeFlattener = new MatTreeFlattener(
      this._transformer,
      node => node.level,
      node => node.expandable,
      node => node.children
    );
    this.treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level,
      node => node.expandable
    );
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    this.ElementFilter = this.formBuilder.group({
      name: "",
      delete_flg: "False",
      user_created: 36
    });
    this.DeleteElement = this.formBuilder.group({
      id: -1,
      name_element: ""
    })
    this.DeletePackage = this.formBuilder.group({
      id: -1,
      delete_flg: "False"
    })
    this.CreateElement = this.formBuilder.group({
      user_created: 36,
      delete_flg: "False",
      name: "",
      version_id: 0,
      package: 11,
      date_create_start: null,
      date_create_end: null
    })
    this.ChangeElement = this.formBuilder.group({
      name: "",
      version_id: -1
    })
  }
  ngOnInit() {
    this.getWbs()
  }

  wbs: Wbs[] = []
  ProjectFilter: ProjectFilter[] = []
  DirectionFilter: DirectionFilter[] = []
  ObjectFilter: ObjectFilter[] = []
  dataSource: MatTreeFlatDataSource<Wbs, ExampleFlatNode>;
  treeControl: FlatTreeControl<ExampleFlatNode>;
  treeFlattener: MatTreeFlattener<Wbs, ExampleFlatNode>;

  getUserPf: any = []
  userPf!: string;
  getPackagePf: any = []
  packagePf!: string;
  getUserDf: any = []
  userDf!: string;
  getPackageDf: any = []
  packageDf!: string;
  getUserObj: any = []
  userObj!: string;
  getPackageObj: any = []
  packageObj!: string;

  ProjectFilters!: FormGroup;
  DirectionFilters!: FormGroup;
  ObjectFilters!: FormGroup;
  SystemFilters!: FormGroup;
  EstimateFilters!: FormGroup;
  WorkFilters!: FormGroup;

  ElementFilter!: FormGroup;
  CreateElement!: FormGroup;
  DeleteElement!: FormGroup;
  DeletePackage!: FormGroup;
  ChangeElement!: FormGroup;

  structure = true;
  table = false;
  showBlock1 = false;
  createAdd = false

  setWidthMin() {
    this.theme.toggleCollapse()
    this.theme.setDrop('drop-dwn');
    this.theme.setMenu('menu-min');
    this.theme.setStyle('btn-brand-close');
  }

  create() {
    this.showBlock1 = !this.showBlock1;
  }
  closed() {
    // Cкрывает все формы
    this.isFormVisible = [false, false, false, false, false, false];
    this.createAdd = false;
  }

  isFormVisible = [false, false, false, false, false, false];
  toggleFormVisibility(index: number): void {
    // Сначала скрыть все формы
    this.isFormVisible = [false, false, false, false, false, false];
    // Затем отобразить выбранную форму
    this.isFormVisible[index] = true;
    this.isElementsListVisible = false;
  }

  elements: WbsElement[] = [];
  currentChangeElement!: WbsElement;
  currentOpenElementsListId = -1;
  isElementsListVisible = false;
  isElementCreateVisible = false;
  isElementChangeVisible = false;
  elementListTitle!: Wbs;
  elementLevel!: number;

  toggleElementsListVisible(title: Wbs, index: number, packageId: number) {
    if (packageId == this.currentOpenElementsListId) {
      this.closeElementList();
    } else {
      this.elementLevel = index;
      this.elements = [];
      this.currentOpenElementsListId = packageId;
      this.isElementsListVisible = true;
      this.elementListTitle = title;
    }

    if (this.isElementsListVisible) {
      switch (index) {
        case 0:
          this.getProjectElement(packageId);
          break;
        case 1:
          this.getDirectionElement(packageId);
          break;
        case 2:
          this.getObjectElement(packageId);
          break;
        case 3:
          this.getSystemElement(packageId);
          break;
        case 4:
          this.getWorkElement(packageId);
          break;
        case 5:
          this.getEstimateElement(packageId);
          break;
        default:
          break;
      }
    }
  }

  closeElementList() {
    this.isElementsListVisible = false;
    this.currentOpenElementsListId = -1;
    this.elementLevel = -1;
    this.elements = [];
  }

  toggleElementCreateVisible() {
    this.isElementCreateVisible = !this.isElementCreateVisible;
  }

  toggleElementChangeVisible(element: WbsElement) {
    this.isElementChangeVisible = !this.isElementChangeVisible;
    this.currentChangeElement = element;
  }

  addElement(level: number, packageId: number) {

    this.elements = [];

    switch (level) {
      case 0:
        this.addProjectElement(packageId);
        break;
      case 1:
        this.addDirectionElement(packageId);
        break;
      case 2:
        this.addObjectElement(packageId);
        break;
      case 3:
        this.addSystemElement(packageId);
        break;
      case 4:
        this.addWorkElement(packageId);
        break;
      case 5:
        this.addEstimateElement(packageId);
        break;
      default:
        break;
    }

    this.toggleElementCreateVisible();
  }

  changeElement(level: number) {
    this.elements = [];

    switch (level) {
      case 0:
        this.changeProjectElement(this.currentChangeElement.id);
        break;
      case 1:
        this.changeDirectionElement(this.currentChangeElement.id);
        break;
      case 2:
        this.changeObjectElement(this.currentChangeElement.id);
        break;
      case 3:
        this.changeSystemElement(this.currentChangeElement.id);
        break;
      case 4:
        this.changeWorkElement(this.currentChangeElement.id);
        break;
      case 5:
        this.changeEstimateElement(this.currentChangeElement.id);
        break;
      default:
        break;
    }

    this.isElementChangeVisible = !this.isElementChangeVisible;
  }

  deleteElement(index: number, element: WbsElement) {

    switch (this.elementLevel) {
      case 0:
        this.deleteProjectElement(element.id, element.name);
        break;
      case 1:
        this.deleteDirectionElement(element.id, element.name);
        break;
      case 2:
        this.deleteObjectElement(element.id, element.name);
        break;
      case 3:
        this.deleteSystemElement(element.id, element.name);
        break;
      case 4:
        this.deleteWorkElement(element.id, element.name);
        break;
      case 5:
        this.deleteEstimateElement(element.id, element.name);
        break;
      default:
        break;
    }

    this.elements.splice(index, 1);
  }

  deletePackage(index?: number) {

    switch (this.elementLevel) {
      case 0:
        this.deleteProjectPackage(index);
        break;
      case 1:
        this.deleteDirectionPackage(index);
        break;
      case 2:
        this.deleteObjectPackage(index);
        break;
      case 3:
        this.deleteSystemPackage(index);
        break;
      case 4:
        this.deleteWorkPackage(index);
        break;
      case 5:
        this.deleteEstimatePackage(index);
        break;
      default:
        break;
    }
  }

  getProjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getProjectElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getDirectionElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getDirectionElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getObjectElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getObjectElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getSystemElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getSystemElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getWorkElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getWorkElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }
  getEstimateElement(packageId: number) {
    const ElementFilter = this.ElementFilter.value;
    this.wbsService.getEstimateElement(ElementFilter).subscribe(
      (response: PackageElements[]) => {
        response[0].data?.forEach(element => {
          console.log(element);
          console.log(packageId)
          if (element.package == packageId) {
            this.elements.push(element);
          }
        })
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  displayedColumns: string[] = ['name', 'count', 'dates'];
  tableViewDisplayedColumn: string[] = ['Проект', 'Направление', 'Объект', 'Система', 'Пакет работ', 'Смета'];
  dataSources: MatTableDataSource<Wbs> = new MatTableDataSource<Wbs>([]);
  tableViewDataSources!: WbsTable[];
  flattenData(data: Wbs[]): Wbs[] {
    let flattenedData: Wbs[] = [];
    data.forEach((item) => {
      flattenedData.push(item);
      if (item.children && item.children.length > 0) {
        flattenedData = flattenedData.concat(this.flattenData(item.children));
      }
    });
    return flattenedData;
  }

  getWbs(): void {
    this.wbsService.getWbs().subscribe(
      (data: Wbs[]) => {
        this.wbs = data;
        this.dataSource.data = data;
        this.dataSources.data = this.flattenData(data);
        this.tableViewDataSources = this.convertToTableView(data);
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  tableRowspanInfo: any = {
    level0: new Array(),
    level1: new Array(),
    level2: new Array(),
    level3: new Array(),
    level4: new Array(),
    level5: new Array()
  };

  convertToTableView(data: Wbs[]) {
    const convertedData: WbsTable[] = [];

    data.forEach((element) => {
      const rowsCount = this.getTableDataRows(element, 0);
      this.tableRowspanInfo.level0.push(rowsCount);
      const level0 = element.name;

      element.children?.forEach(child1 => {
        const level1RowsCount = this.getTableDataRows(child1, 1);
        this.tableRowspanInfo.level1.push(level1RowsCount);
        const level1 = child1.name;

        child1.children?.forEach(child2 => {
          const level2RowsCount = this.getTableDataRows(child2, 2);
          this.tableRowspanInfo.level2.push(level2RowsCount);
          const level2 = child2.name;

          child2.children?.forEach(child3 => {
            const level3RowsCount = this.getTableDataRows(child3, 3);
            this.tableRowspanInfo.level3.push(level3RowsCount);
            const level3 = child3.name;

            child3.children?.forEach(child4 => {
              const level4RowsCount = this.getTableDataRows(child4, 4);
              this.tableRowspanInfo.level4.push(level4RowsCount);
              const level4 = child4.name;

              child4.children?.forEach(child5 => {
                const level5RowsCount = this.getTableDataRows(child5, 5);

                this.tableRowspanInfo.level0.push(0);
                this.tableRowspanInfo.level1.push(0);
                this.tableRowspanInfo.level2.push(0);
                this.tableRowspanInfo.level3.push(0);
                this.tableRowspanInfo.level4.push(0);
                this.tableRowspanInfo.level5.push(level5RowsCount);

                const level5 = child5.name;

                for (let i = 0; i < level5RowsCount; i++) {
                  const row = {
                    level0: {name: level0, package_id: element.id_package, wbs: element},
                    level1: {name: level1, package_id: child1.id_package, wbs: child1},
                    level2: {name: level2, package_id: child2.id_package, wbs: child2},
                    level3: {name: level3, package_id: child3.id_package, wbs: child3},
                    level4: {name: level4, package_id: child4.id_package, wbs: child4},
                    level5: {name: level5, package_id: child5.id_package, wbs: child5}
                  }

                  convertedData.push(row);
                }

              })

              if (!child4.children || child4.children.length <= 0) {
                this.tableRowspanInfo.level0.push(0);
                this.tableRowspanInfo.level1.push(0);
                this.tableRowspanInfo.level2.push(0);
                this.tableRowspanInfo.level3.push(0);
                this.tableRowspanInfo.level4.push(0);
                this.tableRowspanInfo.level5.push(1);

                const row = {
                  level0: {name: level0, package_id: element.id_package, wbs: element},
                  level1: {name: level1, package_id: child1.id_package, wbs: child1},
                  level2: {name: level2, package_id: child2.id_package, wbs: child2},
                  level3: {name: level3, package_id: child3.id_package, wbs: child3},
                  level4: {name: level4, package_id: child4.id_package, wbs: child4},
                  level5: {name: "", package_id: -1, wbs: null}
                }

                convertedData.push(row);
              }

              this.tableRowspanInfo.level4.pop();

            })

            if (!child3.children || child3.children.length <= 0) {
              this.tableRowspanInfo.level0.push(0);
              this.tableRowspanInfo.level1.push(0);
              this.tableRowspanInfo.level2.push(0);
              this.tableRowspanInfo.level3.push(0);
              this.tableRowspanInfo.level5.push(1);
              this.tableRowspanInfo.level4.push(1);

              const row = {
                level0: {name: level0, package_id: element.id_package, wbs: element},
                level1: {name: level1, package_id: child1.id_package, wbs: child1},
                level2: {name: level2, package_id: child2.id_package, wbs: child2},
                level3: {name: level3, package_id: child3.id_package, wbs: child3},
                level4: {name: "", package_id: -1, wbs: null},
                level5: {name: "", package_id: -1, wbs: null}
              }

              convertedData.push(row);
            }

            this.tableRowspanInfo.level3.pop();

          })

          if (!child2.children || child2.children.length <= 0) {
            this.tableRowspanInfo.level0.push(0);
            this.tableRowspanInfo.level1.push(0);
            this.tableRowspanInfo.level2.push(0);
            this.tableRowspanInfo.level3.push(1);
            this.tableRowspanInfo.level4.push(1);
            this.tableRowspanInfo.level5.push(1);

            const row = {
              level0: {name: level0, package_id: element.id_package, wbs: element},
              level1: {name: level1, package_id: child1.id_package, wbs: child1},
              level2: {name: level2, package_id: child2.id_package, wbs: child2},
              level3: {name: "", package_id: -1, wbs: null},
              level4: {name: "", package_id: -1, wbs: null},
              level5: {name: "", package_id: -1, wbs: null}
            }

            convertedData.push(row);
          }

          this.tableRowspanInfo.level2.pop();

        })

        if (!child1.children || child1.children.length <= 0) {
          this.tableRowspanInfo.level0.push(0);
          this.tableRowspanInfo.level1.push(0);
          this.tableRowspanInfo.level2.push(1);
          this.tableRowspanInfo.level3.push(1);
          this.tableRowspanInfo.level4.push(1);
          this.tableRowspanInfo.level5.push(1);

          const row = {
            level0: {name: level0, package_id: element.id_package, wbs: element},
            level1: {name: level1, package_id: child1.id_package, wbs: child1},
            level2: {name: "", package_id: -1, wbs: null},
            level3: {name: "", package_id: -1, wbs: null},
            level4: {name: "", package_id: -1, wbs: null},
            level5: {name: "", package_id: -1, wbs: null}
          }

          convertedData.push(row);
        }

        this.tableRowspanInfo.level1.pop();

      })

      if (!element.children || element.children.length <= 0) {
        this.tableRowspanInfo.level0.push(0);
        this.tableRowspanInfo.level1.push(1);
        this.tableRowspanInfo.level2.push(1);
        this.tableRowspanInfo.level3.push(1);
        this.tableRowspanInfo.level4.push(1);
        this.tableRowspanInfo.level5.push(1);

        const row = {
          level0: {name: level0, package_id: element.id_package, wbs: element},
          level1: {name: "", package_id: -1, wbs: null},
          level2: {name: "", package_id: -1, wbs: null},
          level3: {name: "", package_id: -1, wbs: null},
          level4: {name: "", package_id: -1, wbs: null},
          level5: {name: "", package_id: -1, wbs: null}
        }

        convertedData.push(row);
      }

      this.tableRowspanInfo.level0.pop();

    })

    return convertedData;
  }

  getRowspan(column: string, index: number) {
    return this.tableRowspanInfo[column][index];
  }

  getTableDataRows(data: Wbs, level: number) {
    let row = (data.children && data.children.length > 0) ? 0 : 1;

    data.children?.forEach((children) =>
      row += this.getTableDataRows(children, level + 1)
    )

    return row;
  }

  addProjectFilter() {
    const ProjectFilter = this.ProjectFilters.value;
    this.wbsService.addProjectFilter(ProjectFilter).subscribe(
      (response: ProjectFilter[]) => {
        console.log('Data added successfully:', response);
        this.ProjectFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addDirectionFilter() {
    const DirectionFilter = this.DirectionFilters.value;
    this.wbsService.addDirectionFilter(DirectionFilter).subscribe(
      (response: DirectionFilter[]) => {
        console.log('Data added successfully:', response);
        this.DirectionFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addObjectFilter() {
    const ObjectFilter = this.ObjectFilters.value;
    this.wbsService.addObjectFilter(ObjectFilter).subscribe(
      (response: ObjectFilter[]) => {
        console.log('Data added successfully:', response);
        this.ObjectFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addSystemFilter() {
    const SystemFilter = this.SystemFilters.value;
    this.wbsService.addSystemFilter(SystemFilter).subscribe(
      (response: SystemFilter[]) => {
        console.log('Data added successfully:', response);
        this.SystemFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addWorkFilter() {
    const WorkFilter = this.WorkFilters.value;
    this.wbsService.addWorkFilter(WorkFilter).subscribe(
      (response: WorkFilter[]) => {
        console.log('Data added successfully:', response);
        this.WorkFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addEstimateFilter() {
    const EstimateFilter = this.EstimateFilters.value;
    this.wbsService.addEstimateFilter(EstimateFilter).subscribe(
      (response: EstimateFilter[]) => {
        console.log('Data added successfully:', response);
        this.EstimateFilters.reset();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteProjectPackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteProjectPackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.isElementsListVisible = false;
        this.getWbs();
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteDirectionPackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteDirectionPackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteObjectPackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteObjectPackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteSystemPackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteSystemPackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteWorkPackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteWorkPackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteEstimatePackage(id?: number) {
    const DeletePackage = this.DeletePackage.value;
    this.wbsService.deleteEstimatePackage(DeletePackage, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeProjectElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeProjectElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getProjectElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeDirectionElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeDirectionElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getDirectionElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeObjectElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeObjectElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getObjectElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeSystemElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeSystemElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getSystemElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeWorkElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeWorkElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getWorkElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  changeEstimateElement(id: number) {
    const ChangeElement = this.ChangeElement.value;
    this.wbsService.changeEstimateElement(ChangeElement, id).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getEstimateElement(this.elementListTitle.id);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addProjectElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addProjectElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getProjectElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addDirectionElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addDirectionElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getDirectionElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addObjectElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addObjectElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getObjectElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addSystemElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addSystemElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getSystemElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addWorkElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addWorkElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getWorkElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  addEstimateElement(index: number) {
    const CreateElement = this.CreateElement.value;
    CreateElement.package = index;
    this.wbsService.addEstimateElement(CreateElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        this.getEstimateElement(index);
        this.CreateElement.reset()
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteProjectElement(id: number, name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteProjectElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteDirectionElement(id: number, name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteDirectionElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteObjectElement(id: number,name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteObjectElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteSystemElement(id: number,name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteSystemElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteWorkElement(id: number,name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteWorkElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  deleteEstimateElement(id: number,name: string) {
    const DeleteElement = this.DeleteElement.value;
    DeleteElement.id = id;
    DeleteElement.name_element = name
    this.wbsService.deleteEstimateElement(DeleteElement).subscribe(
      (response: Object) => {
        console.log('Data delete successfully:', response);
        // Handle success case
      },
      (error: any) => {
        console.error('Failed to add data:', error);
        // Handle error case
      }
    );
  }
  private _transformer = (node: Wbs, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
      id: node.id_package,
      type_node: node.type_node,
      start_date: node.start_date,
      end_date: node.end_date,
      percent_date: node.percent_date,
      code_wbs: node.code_wbs
    };
  };

  hasChild(_: number, node: ExampleFlatNode): boolean {
    return node.expandable;
  }

  protected readonly alert = alert
}
