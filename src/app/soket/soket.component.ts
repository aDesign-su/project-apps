import { Component, OnInit } from '@angular/core';
import { io } from 'socket.io-client';

const SOCKET_ENDPOINT = 'http://localhost:8000:80';

@Component({ 
  selector: 'app-soket',
  templateUrl: './soket.component.html',
  styleUrls: ['./soket.component.css']
})
export class SoketComponent implements OnInit {
  title = 'Angular Chat App';
  message!: string;
  messages: string[] = []; 
  socket: any;  
   /** info text */
  ngOnInit() {  
    this.socket = io(SOCKET_ENDPOINT);
      
    this.socket.on('message', (msg: any) => {
      this.messages.push(msg);
    });    
/** info */
    this.socket.on('connect', () => { 
      console.log('Соединение с сервером WebSocket установлено.');
    });  
    this.socket = io(SOCKET_ENDPOINT, { withCredentials: true });

    this.socket.on('message', (data: any) => {
      console.log('Получено сообщение:', data);
      // Обработайте полученные данные здесь, например, обновите интерфейс
    });
  }

  sendMessage() {
    this.socket.emit('message', this.message);
    this.message = '';
  }
}
