export interface Wbs {
    id: number,
    name: string,
    id_package: number,
    delete_flg: boolean,
    lft: number,
    rght: number,
    tree_id: number,
    level: number,
    parent: number,
    name_package: string,
    type_node: string,
    code_wbs: string,
    start_date: string,
    end_date: string,
    percent_date: string,
    name_level: string,
    children?: Wbs[]
}
export interface WbsTable {
  level0: WbsTableElement;
  level1: WbsTableElement;
  level2: WbsTableElement;
  level3: WbsTableElement;
  level4: WbsTableElement;
  level5: WbsTableElement;
}
export interface WbsTableElement {
  name: string,
  package_id: number,
  wbs: Wbs|null
}
export interface ProjectFilter {
    id: number,
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
}
export interface DirectionFilter {
    id: number,
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number,
}
export interface ObjectFilter {
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number
}
export interface SystemFilter {
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number
}
export interface WorkFilter {
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number
}
export interface EstimateFilter {
    date_created: string,
    name: string,
    delete_flg: false,
    version_id: number,
    type_version: string,
    user_created: number,
    package: number
}
export interface PackageElements {
  data?: WbsElement[],
  users?: User[]
}
export interface WbsElement {
  id:           number;
  date_created: Date;
  name:         string;
  delete_flg:   boolean;
  version_id:   number;
  user_created: number;
  package:      number;
}
export interface CreateElement {
  user_created: number,
  delete_flg: string,
  name: string,
  version_id: number,
  package: number,
  date_create_start: any,
  date_create_end: any
}
export interface User {
  id: number,
  username: string,
  first_name: string,
  last_name: string
}
export interface ElementFilter {
  name: string,
  user_created: number
}
export interface DeleteEntity {
  id: number,
  delete_flg: string
}
export interface ChangeElement {
  name: string,
  version_id: number
}
